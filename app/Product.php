<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [ 'categorie_id', 'name', 'image','price', 'description' ];

  public function category() {
      return $this->belongsTo('App\Category');
  }
}
