<?php

namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    

    public function index()
    {
        $categories = Category::all();
        return response()->json($categories);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function create()
    {
        $categories = Category::all();
        return response()->json($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $category = new Category;
      $category->name = $request->name;
      $category->parent_id = $request->parent_category ? $request->parent_category : 0;

      if ($category->save() ) {
        return response()->json($category);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\rc  $rc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  Category $category)
    {
      $category->name = $request->name;
      $category->parent_id = $request->parent_category ? $request->parent_category : 0;
      if ($category->save() ) {
        return response()->json($category);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\rc  $rc
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorie = Category::findOrFail($id)->delete();
        return response()->json($categorie);
    }

    public function findCategorie($categorie)
    {
            $categorie = Category::where('name', 'LIKE', '%'.$categorie. '%')
        
            ->paginate(10);

        return response()->json($categorie);
    }
}