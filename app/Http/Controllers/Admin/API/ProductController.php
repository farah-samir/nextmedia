<?php

namespace App\Http\Controllers\Admin\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;


class ProductController extends Controller
{
 

    public function index()
    {
        $products = Product::with('category')->get();
        return response()->json($products);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //  $categories = Categorie::with('children')->where('parent_id')->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exploaded = explode(',', $request->image);
        $decoded = base64_decode($exploaded[1]);
        if(str_contains($exploaded[0],'jpeg'))
        $extension = 'jpg';
        else 
        $extension = 'png';
        $fileName = str_random().'.'.$extension;
        $path = public_path().'/'.$fileName;
        file_put_contents($path,$decoded);
        $product = Product::create($request->except('image') + [
            'image' => $fileName
            ]);
      
    }
    public function destroy($id)
    {
        $product = Product::findOrFail($id)->delete();
        return response()->json($product);
    }
   

}
