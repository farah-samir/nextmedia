import Dashboard from "./components/Dashboard";
import Profile from "./components/Profile";
import Index from "./components/Users/Index";
import NotFound from "./components/NotFound";
import Product from "./components/Product";

import Categorie from "./components/Categorie";
export default {
    mode: "history",
    routes: [
        {
            path: "/admin/dashboard",
            component: Dashboard
        },
        {
            path: "/admin/profile",
            component: Profile
        },
        {
            path: "/admin/users",
            component: Index
        },
      
        {
            path: "/admin/categories",
            component: Categorie
        },
        {
          path: "/admin/products",
          component: Product
      },

        {
            path: "*",
            component: NotFound
        }
    ]
};
